#include <stdio.h>
#include <stdlib.h>

void success() {
    printf("Bravo, vous avez réussi !!\n");
}

int main() {
    int v = 0xaaaaaaaa;
    char string[16];
    printf("Quel est le mot de passe ? ");
    scanf("%s", &string);
    if (v == 0xdeadbeef) {
        success();
    } else {
        printf("%s n'est pas le bon mot de passe...\n", string);
    }

    return 0;
}

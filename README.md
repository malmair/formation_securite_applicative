# Formation Sécurité Applicative

Ce repo est destiné aux personnes intéressées par la formation sur la sécurité applicative 2020.

## Minisite de la formation

Le minisite de la formation est trouvable ici : https://www.bde.enseeiht.fr/~malmair/formations/securite_applicative_2020/

## VM de travail

Une machine virtuelle a été créée pour les TP de la formation.
La machine virtuelle est disponible sur le minisite de la formation.
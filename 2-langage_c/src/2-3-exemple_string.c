#include <stdio.h>

int main() {
    char mon_nom = 'e';
    char string[4] = "abc"; // à quoi correspond réellement ce "..." ?
    printf("0x%llx\n", string); // Qu'est ce qui s'affiche ?
    
    char caractere_2 = *(string + 2);
    printf("%c\n", caractere_2); // Qu'est-ce qui s'affiche ?

    *(string+3) = 'd'; // équivalent à string[3] !
    printf("%s\n", string); // qu'est ce qui va être affiché ?

    return 0;
}

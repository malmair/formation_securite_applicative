#include <stdio.h>

void ma_fonction(int i) {
    i++;
}

void ma_fonction_pointeur(int* i) {
    (*i)++;
}

int main() {
    int a = 0;
    int *p;
    p = &a;
    *p = 1;

    printf("%d\n", a); // qu'est-ce qui est affiché ?

    ma_fonction(a);

    printf("%d\n", a); // qu'est-ce qui est affiché ?

    ma_fonction_pointeur(p);

    printf("%d\n", a); // qu'est-ce qui est affiché ?

    ma_fonction_pointeur(&a);

    printf("%d\n", a); // qu'est-ce qui est affiché ?

    return 0;
}

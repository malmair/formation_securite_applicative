#include <stdio.h>

int main() {
    int a = 5;
    int *p = &a, *p2 = p+1;

    printf("0x%llx\n", p);
    printf("0x%llx\n", p2);
    // Que vaut p2 - p en octet ?

    return 0;
}

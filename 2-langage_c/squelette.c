// Importation des bibliothèques
#include <stdio.h> // Gestion des input / output : scanf, fgets, printf, etc...
#include <stdlib.h> // En vrac : allocation dynamique, system, exit, rand, conversion chaîne de caractère / entiers, etc...

//Entête de la fonction principale
//arguments facultatifs, argc = nombre d'argumentgs, argv = tableau contenant les arguments sous forme de strings
int main (int argc, char* argv[]) {
    
    // Votre code ici...

    return 0; //facultatif, indique que le programme s'est bien terminé. si un entier autre que 0 est renvoyé, une erreur a eu lieu pendant l'exécution.
}

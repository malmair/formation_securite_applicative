#include <stdio.h>

int main() {
    for (int i=0; i<0x100; ++i) {
        if (i%0x10 != 0) {
            printf("%c\n", i);
        }
    }

    return 0;
}

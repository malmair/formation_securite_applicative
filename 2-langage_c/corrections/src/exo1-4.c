#include <stdio.h>
#include <string.h>

int main() {
    char s[5];
    int entier = 0;

    printf("Votre nombre hexadécimal ? (format, avec lettre minuscules : 0x**) ");
    scanf("%s", &s);
    
    if (strlen(s) != 4) {
            printf("Le format n'est pas reconnu...\n");
            return 1;
    }

    for (int i=0; i<2; ++i) {
        char chiffre = s[2+i];
        int facteur = (i == 0) ? 16 : 1;
        if (chiffre - '0' >=0 && chiffre - '0' <= 9) {
            entier += facteur*(chiffre - '0');
        } else if (chiffre - 'a' >=0 && chiffre - 'a' <= 5) {
            entier += facteur*(10+(chiffre-'a'));
        } else {
            printf("Le format n'est pas reconnu...\n");
            return 1;
        }
    }

    printf("Votre nombre est %d\n", entier);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    
    char prenom[50];
    printf("Quel est votre prénom ?\n");

    // Ici pas mal de possibilités
    // Regarder la liste des fonctions possible dans stdio : https://koor.fr/C/cstdio/cstdio.wp
    //scanf("%s", &prenom); -> classique
    //fgets(prenom, 50, stdin); -> attention, le caractère retour à la ligne est également enregistré !

    // un méthode un peu moins triviale, essayez de suivre
    char c;
    int i = 0;
    while ((c = getchar()) != '\n' && c != EOF && i < 50) {
        prenom[i++] = c;
    }
    prenom[i] = '\0'; //Ne pas oublier !!

    printf("Votre prénom est %s\n", prenom);

    return 0;
}

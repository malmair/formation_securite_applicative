#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define TAILLE_INIT_PILE 1


typedef struct pile pile;

struct pile {
    int *tblx;
    int taille;
    int max;
};

void initialiser (pile *p_ptr) {
    p_ptr->taille = 0;
    p_ptr->max = TAILLE_INIT_PILE;
    p_ptr->tblx = (int*) malloc(p_ptr->max * sizeof(int));
    if(p_ptr->tblx == NULL) {
        printf("Erreur lors de l'allocation du tableau.\n");
        exit(1);
    }
}

void empiler (pile *p_ptr, int i) {
    if (p_ptr->taille == p_ptr->max) {
        p_ptr->max *= 2;
        p_ptr->tblx = (int*) realloc(p_ptr->tblx, p_ptr->max*sizeof(int));
        if(p_ptr->tblx == NULL) {
            printf("Erreur lors de l'allocation du tableau.\n");
            exit(1);
        }
    }

    p_ptr->tblx[(p_ptr->taille)++] = i;
}

int depiler (pile *p_ptr) {
    if (p_ptr->taille == 0) {
        printf("Erreur : La pile est vide.\n");
        exit(1);
    }

    return p_ptr->tblx[--(p_ptr->taille)];
}

bool est_vide(pile* p_ptr) {
    return p_ptr->taille == 0;
}

int top(pile* p_ptr) {
    return p_ptr->tblx[(p_ptr->taille)-1];
}

void afficher(pile *p_ptr) {
    int i = (p_ptr->taille) - 1;
    printf("=== TOP OF THE STACK ===\n");
    while(i != -1) {
        printf("%d\n", p_ptr->tblx[i--]);
    }
    printf("=== BOTTOM OF THE STACK ===\n"); 
}

int main() {
    pile p;
    initialiser(&p);

    empiler(&p, 1);

    afficher(&p);

    printf("\n\n");

    empiler(&p, 2);

    empiler(&p, 3);
    
    afficher(&p);
    printf("\n\n");

    printf("depilement : %d\n", depiler(&p));

    afficher(&p);
    printf("\n\n");

    empiler(&p, 4);

    afficher(&p);
    printf("\n\n");

    printf("TOP = %d\n", top(&p));

    printf("Est vide ? %s\n", est_vide(&p) ? "oui" : "non"); // opérateur ternaire : a ? b : c <=> si a est vrai alors b sinon c
    printf("\n\n");

    depiler(&p);
    depiler(&p);
    depiler(&p);

    afficher(&p);
    printf("\n\n");

    printf("Est vide ? %s\n", est_vide(&p) ? "oui" : "non");
    depiler(&p);

    return 0;
}

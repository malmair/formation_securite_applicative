#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    
    int counter = 0;
    for(int i=1; i<101; i++) { //++i fonctionne également
        counter += i;
    }

    printf("%d\n", counter);

    return 0;
}
